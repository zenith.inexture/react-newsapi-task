import { useLocation } from "react-router-dom";

function DisplayInfo() {
  const location = useLocation();
  return (
    <>
      <div className="display_info">
        <p className="dispaly_author">Author : {location.state.array.author}</p>
        <p className="dispaly_content">
          Content : {location.state.array.content}
        </p>
        <p className="dispaly_description">
          Description :{location.state.array.description}
        </p>
        <p className="dispaly_publishedAt">
          PublishedAt :{location.state.array.publishedAt}
        </p>
        <p className="dispaly_title">Title : {location.state.array.title}</p>
        <a className="dispaly_url" href={location.state.array.url}>
          More Information
        </a>
        <img
          src={location.state.array.urlToImage}
          alt={location.state.array.urlToImage}
        ></img>
      </div>
    </>
  );
}
export default DisplayInfo;
