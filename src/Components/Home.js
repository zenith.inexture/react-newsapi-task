import "../App.css";
import React from "react";
import { useNavigate } from "react-router-dom";

function Home() {
  const navigate = useNavigate();
  const [name, setName] = React.useState("");
  const [search, setSearch] = React.useState("");
  const [originaldata, setOriginaldata] = React.useState([]);
  const [data, setData] = React.useState([]);
  const [slicevalue, setSlicevalue] = React.useState(2);

  const handlesearch = (e) => {
    setSearch(e.target.value);
    const filtereddata = originaldata.filter((i) => {
      return i.title.toLowerCase().includes(e.target.value.toLowerCase());
    });
    setData(filtereddata);
    setSlicevalue(2);
  };

  const handlesubmit = () => {
    fetch(
      `https://newsapi.org/v2/everything?q=${name}&apiKey=d3a68d3a93a54948a016a1553bc4d20c`
    )
      .then((response) => response.json())
      .then((data) => {
        setData(data.articles);
        setOriginaldata(data.articles);
        console.log(data.articles);
      })
      .catch((error) => console.log(error));
    setSlicevalue(2);
  };
  const handleincrease = () => {
    setSlicevalue((prevalue) => prevalue + 2);
  };
  const handledecrease = () => {
    setSlicevalue((prevalue) => prevalue - 2);
  };
  const handlerowclick = (value) => {
    navigate("displayinfo", { state: { array: value } });
  };
  return (
    <div className="App">
      <label>
        Keyword :
        <input
          type="text"
          onChange={(e) => setName(e.target.value)}
          value={name}
          className="all_input"
        />
      </label>
      <button type="submit" onClick={handlesubmit} className="all_btn">
        Search
      </button>
      <br></br>
      {originaldata.length !== 0 && (
        <>
          <label>
            Search by Title:
            <input
              type="text"
              onChange={(e) => handlesearch(e)}
              value={search}
              className="all_input"
            ></input>
          </label>
          <table className="table_css">
            <thead className="table_head">
              <tr>
                <th>NO.</th>
                <th>Author</th>
                <th>Description</th>
                <th>Title</th>
              </tr>
            </thead>
            <tbody className="table_body">
              {data.slice(0, slicevalue).map((i, index) => {
                return (
                  <tr
                    key={index}
                    onClick={() => handlerowclick(i)}
                    className="body_row"
                  >
                    <td>{index + 1}</td>
                    <td>{i.author}</td>
                    <td>{i.description}</td>
                    <td>{i.title}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          {data.length === 0 ||
          slicevalue >= data.length ||
          slicevalue === 0 ||
          data.length < 2 ? null : (
            <button type="submit" onClick={handleincrease} className="all_btn">
              Load more data
            </button>
          )}
          {slicevalue > 3 ? (
            <button type="submit" onClick={handledecrease} className="all_btn">
              decrease Data Load
            </button>
          ) : null}
        </>
      )}
    </div>
  );
}

export default Home;
