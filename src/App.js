import { Route, Routes } from "react-router-dom";
import React from "react";
import Home from "./Components/Home";
const LazyDisplyInfo = React.lazy(() => import("./Components/DisplayInfo"));
function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route
          path="displayinfo"
          element={
            <React.Suspense fallback="Loading...">
              <LazyDisplyInfo />
            </React.Suspense>
          }
        ></Route>
      </Routes>
    </>
  );
}

export default App;
